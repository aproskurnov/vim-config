set encoding=utf-8

set nu
"indent
set tabstop=4
set softtabstop=4
set shiftwidth=4

set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
"Plugin install
Plugin 'gmarik/Vundle.vim'
Plugin 'tomtom/tlib_vim'
Plugin 'tomtom/tskeleton_vim'
Plugin 'daylerees/colour-schemes', {'rtp':'vim-themes/'}
Plugin 'altercation/vim-colors-solarized'
Plugin 'tpope/vim-surround'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'
Plugin 'shawncplus/phpcomplete.vim'
Plugin 'mattn/emmet-vim'
call vundle#end()

"mapping
let mapleader = ","
nmap <leader>ne :NERDTreeToggle<cr>
imap <C-b> public function 

filetype plugin indent on
"set omnifunc=syntaxcomplete#Complete

"search
set ignorecase
set smartcase

"tskeleton
autocmd BufNewFile *.php TSkeletonSetup template.php
autocmd BufNewFile *.py TSkeletonSetup template.py
let g:tskelUserName = "Aleksey Proskurnov"
let g:tskelUserEmail = "proskurn@gmail.com"
let g:tskelDateFormat = "%Y-%m-%d"

"common colorscheme
set t_Co=256
syntax on 

"colorscheme solarized setting
set background=dark
let g:solarized_termcolors = 256
let g:solarized_visibility = "high"
let g:solarized_contrast = "high"
colorscheme desert "solarized

"highlight twig/jinja
au BufRead,BufNewFile *.twig set filetype=html
